<?php
use \App\Movie;

Route::get('/', function () {
    return view('welcome');
});

Route::get('ruta-en-laravel', function () {
    //resources/view/hola.blade.php
    return view('hola');
});

Route::get('saludar/{nombre}', function ($nombre) {
    return "<h1>Hola $nombre</h1>";
});

Route::get('multiplicar/{n1}/{n2}', function ($n1, $n2) {
    return $n1 * $n2;
});

Route::get('sumar/{n1}/{n2?}', function ($n1, $n2=0) {
    return $n1 + $n2;
});

Route::get('rand/{n1}/{n2?}', function ($n1, $n2=null) {
    if (is_null($n2)) {
        $n2 = rand($n1+1, $n1+100);
    }
    return rand($n1, $n2);
});

Route::post('ruta-por-post', function () {
    return 'Hola desde post';
});

Route::get('form', function () {
    return '<form action="/ruta-por-post" method="post"><input /></form>';
});

/*
Route::get('buscar/{busqueda}', function ($busqueda) {
    $nombres = [
        'Barry',
        'Barclay',
        'Bert',
        'Bort'
    ];
    
    $resultado = "El nombre no está en el array";
    
    foreach ($nombres as $key => $value) {
        if ($busqueda == $value) {
            $resultado = "$value está en la posición $key";
            break;
        }
    }
    
    return $resultado;
});
*/

Route::get('buscar/{busqueda}', 'BuscarController@buscarNombre');
Route::get('contactenos', 'VistasController@contactenos');
Route::get('usuarios-conectados', 'VistasController@usuariosConectados');
Route::get('ejercicios/clase2_1/{id}', 'EjerciciosController@clase2_1');
Route::get('ejercicios/clase2_2/{nombre}', 'EjerciciosController@clase2_2');
Route::get('ejercicios/clase2_3', 'EjerciciosController@clase2_3');

Route::get('blade', 'BladeController@herencia');

Route::get('peliculas', 'MovieController@index');
Route::get('mostrar-form', 'MovieController@mostrarForm');
Route::post('guardar-form', 'MovieController@guardarForm');
Route::get('mostrar-imagen/{src}', 'MovieController@mostrarImagen');

Route::get('eloquent/all', function () {
    //SELECT * from movies;
    $movies = Movie::all();
    dd($movies);
});

Route::get('eloquent/find/{id}', function ($id) {
    //SELECT * from movies WHERE id = ?;
    $movie = Movie::find($id);
    return $movie->presentarse();
});

Route::get('eloquent/where/get', function () {
    //SELECT * from movies WHERE title = ?;
    $movies = Movie::where('title', '=', 'Toy Story')->get();
    dd($movies);
});

Route::get('eloquent/where/first', function () {
    //SELECT * from movies WHERE title = ? LIMIT 1;
    $movie = Movie::where('title', '=', 'Toy Story')->first();
    dd($movie);
});

Route::get('eloquent/buscar/{texto}', function ($texto) {
    //SELECT * from movies WHERE title = ?;
    //$movies = Movie::where('title', '=', 'Toy Story')->get();
    $movies = Movie::where('title', 'like', "%$texto%")
        ->limit(3)
        ->orderBy('title', 'desc')
        ->get()
    ;
    
    $resultado = '';
    foreach ($movies as $movie) {
        $resultado .= $movie->title . '<br>';
    }
    return $resultado;
});

Route::get('eloquent/where/and', 'EloquentController@whereAnd');
Route::get('eloquent/where/parentesis', 'EloquentController@whereParentesis');
Route::get('eloquent/belongsTo', 'EloquentController@belongsTo');
Route::get('eloquent/hasMany', 'EloquentController@hasMany');
Route::get('eloquent/belongsToMany', 'EloquentController@belongsToMany');
Route::get('eloquent/relationQuery', 'EloquentController@relationQuery');





































