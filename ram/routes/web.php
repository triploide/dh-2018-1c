<?php

Route::get('/', function () {
    return view('welcome');
})->middleware('auth');


//Front
Route::get('series', 'Front\SerieController@index');

Route::get('carrito', 'Front\CartController@show');
Route::get('cart/add/{id}', 'Front\CartController@add');
Route::get('cart/destroy', 'Front\CartController@destroy');
//----------


Route::view('api-test-form', 'api-test-form');

Route::view('react', 'react');

//Route::resource('admin/movies', 'Admin\MovieController');
//admin/movies - listado - GET - index
//admin/movies/{id} - mostrar pelicula por id - GET - show
//admin/movies/create - form de creacion - GET - create
//admin/movies - action de creacion - POST - store
//admin/movies/{id}/edit - form de edicion por id - GET - edit
//admin/movies/{id} - action de edicion por id - PUT / PATCH - update
//admin/movies/{id} - eliminar - DELETE - destroy

Route::get('admin/genres', 'Admin\GenreController@index');


Auth::routes();

Route::get('panel', 'Auth\LoginController@showLoginForm');

Route::get('/home', 'HomeController@index')->name('home');
