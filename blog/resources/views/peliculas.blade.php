<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Películas</title>
    <link rel="stylesheet" href="/css/app.css">
    <style media="screen">
        body {
            padding: 40px
        }
    </style>
</head>
<body>
    <h1>Películas</h1>
    @foreach ($peliculas as $pelicula)
        <p>{{ $pelicula->title }}</p>
    @endforeach
    
    {{ $peliculas->links() }}
</body>
</html>