<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title', 'Blog')</title>
</head>
<body>
    <nav>
        <ul>
            <li><a href="#">Uno</a></li>
            <li><a href="#">Dos</a></li>
            <li><a href="#">Tres</a></li>
        </ul>
    </nav>
    
    <ul>
        @section('breadcrumbs')
            <li>Home</li>
            <li>Categoría</li>
        @show
    </ul>
    
    
    <main>
        @yield('contenido')
    </main>
    
    <footer>
        <p>FOOTER  - Lorem ipsum dolor sit amet, consectetur adipisicing elit. Soluta, nesciunt.</p>
    </footer>
    
</body>
</html>