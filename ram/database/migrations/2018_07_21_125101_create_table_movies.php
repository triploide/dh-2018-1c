<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableMovies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movies', function (Blueprint $table) {
            //->default('valor por defecto')
            $table->mediumIncrements('id');
            $table->string('title');
            $table->text('description')->nullable();
            $table->date('release_date');
            $table->float('rating', 4, 2);
            $table->softDeletes();
            $table->timestamps();
            
            $table->tinyInteger('genre_id')->unsigned()->index();
            //$table->foreign('genre_id')->references('id')->on('genres');
            //$table->unique(['nombre', 'apellido']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movies');
    }
}
