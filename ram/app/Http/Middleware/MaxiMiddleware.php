<?php

namespace App\Http\Middleware;

use Closure;

class MaxiMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ( $request->method() == 'POST') {
            return $next($request);
        } else {
            return redirect('/');
        }
    }
}
