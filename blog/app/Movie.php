<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{
    //protected $table = 'movies'; la tabla es el plural del nombre de la clase
    
    //public $timestamps = false; Si no tengo las marcas de tiempo (created_at, updated_at)
    
    protected $fillable = ['title','rating','awards','release_date', 'genre_id'];
    
    public function presentarse()
    {
        return 'Título: ' . $this->title . ' - Premios: ' . $this->awards;
    }
    
    public function genre()
    {
        //genre_id
        //return $this->belongsTo(Genre::class, 'id_genre', 'key');
        return $this->belongsTo(Genre::class);
    }
    
    public function actors()
    {
        //return $this->belongsToMany(Actor::class, 'tabla_relacion');
        return $this->belongsToMany(Actor::class);
    }
}





