<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Movie;

class MovieController extends Controller
{
    public function index()
    {
        $movies = Movie::all();
        //return $movies->toJson();
        return $movies;
    }
    
    public function show($id)
    {
        return Movie::find($id);
    }
    
    public function store()
    {
        //return request()->user();
        $movie = Movie::create(request()->all());
        return $movie->id;
    }
}





