<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Formulario de Películas</title>
    <link rel="stylesheet" href="/css/app.css">
    <style media="screen">
            body {
                padding: 40px
            }
    </style>
</head>
<body>
    <main class="container">
        @if (count($errors))
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <p>{{ $error }}</p>
                @endforeach
            </div>
        @endif
        <form action="/guardar-form" method="post" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="title">Título</label>
                <input type="text" value="{{old('title')}}" name="title" id="title" class="form-control">
            </div>
            <div class="form-group">
                <label for="awards">Premios</label>
                <input type="text" name="awards" value="{{old('awards')}}" id="awards" class="form-control">
            </div>
            <div class="form-group">
                <label for="rating">Rating</label>
                <input type="text" name="rating" value="{{old('rating')}}" id="rating" class="form-control">
            </div>
            <div class="form-group">
                <label for="release_date">Fecha de estreno</label>
                <input type="date" name="release_date" value="{{old('release_date')}}" id="release_date" class="form-control">
            </div>
            <div class="form-group">
                <label for="genre">Género</label>
                <select class="form-control" name="genre_id" id="genre">
                    @foreach ($genres as $genre)
                        <option value="{{$genre->id}}">{{ $genre->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="banner">Banner</label>
                <input type="file" name="banner" value="" id="banner" class="form-control">
            </div>
            <div class="form-group">
                <label for="actors">Actores</label>
                @foreach ($actors as $actor)
                    <label for="actor{{$actor->id}}">{{$actor->first_name}}</label>
                    <input id="actor{{$actor->id}}" type="checkbox" name="actors[]" value="{{$actor->id}}">
                    <br>
                @endforeach
            </div>
            <div class="form-group">
                <input type="submit" name="enviador" value="Enviar" class="btn btn-primary">
            </div>
        </form>
    </main>
</body>
</html>