<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class EjerciciosController extends Controller
{
    public function clase2_1($id)
    {
        $peliculas = [
            1 => 'Kung Fury',
            2 => 'Triangle',
            3 => 'Coherence'
        ];
        
        return view('ejercicios/clase2_1', ['peli' => $peliculas[$id]]);
        
        //return $peliculas[$id];
    }
    
    public function clase2_2($nombre)
    {
        $peliculas = [
            1 => 'Kung Fury',
            2 => 'Triangle',
            3 => 'Coherence'
        ];
        
        $respuesta = 'No existe la película';
        
        foreach ($peliculas as $pelicula) {
            echo '.';
            if ($pelicula == $nombre) {
                $respuesta =  $nombre;
                break;
            }
        }
        
        return $respuesta;
        
    }
    
    public function clase2_3()
    {
        $peliculas = [
            1 => 'Kung Fury',
            2 => 'Triangle',
            3 => 'Coherence'
        ];
        
        $peliculas = [];
        
        return view('clase2_3', ['pelis' => $peliculas]);
    }
}
