<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Usuarios Conectados</title>
</head>
<body>
    <h1>Usuarios Conectados</h1>
    
    <!--
    <?php foreach($usuarios as $usuario): ?>
        <p>{{ $usuario }}</p>
    <?php endforeach ?>
    -->
    
    @foreach($usuarios as $usuario)
        @if($usuario == 'Bort')
            <p><strong>{{ $usuario }}</strong></p>
        @else
            <p>{{ $usuario }}</p>
        @endif
    @endforeach
    
    
    
</body>
</html>