<?php

use Faker\Generator as Faker;

$factory->define(App\Movie::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence(3),
        'description' => $faker->sentence(3),
        'stars' => rand(1, 5),
        'release_date' => $faker->date(),
        'genre_id' => rand(1, 3),
    ];
});

//factory(App\Movie::class)->make();
//factory(App\Movie::class)->create();

