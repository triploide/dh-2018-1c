<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{
    protected $fillable = ['title', 'description', 'release_date', 'genre_id', 'stars'];
    
    protected $hidden =  ['stars', 'release_date', 'genre_id'];
    
    public function genre()
    {
        return $this->belongsTo(Genre::class);
    }
}
