<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Movies</title>
    <link rel="stylesheet" href="/css/app.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
</head>
<body>
    
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="#">
          <img src="/images/logo.png" width="150" alt="">
        </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="nav-link" href="/admin">Home <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/admin/movies">Movies</a>
          </li>
        </ul>
      </div>
      
      <a class="navbar-brand" href="#">
          {{--
        @if (Auth::check())
            Hola {{ Auth::user()->name }}
        @else
            Hola Usuario
        @endif
        --}}
        @auth
            Hola {{ Auth::user()->name }}
        @else
            Hola Usuario
        @endauth
      </a>
    </nav>
    
    <main class="container">
        <h1>Movies</h1>
        
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>Título</th>
                    <th>Género</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($pelis as $movie)
                    <tr>
                        <td>{{ $movie->title }}</td>
                        <td>{{ $movie->genre->name }}</td>
                        <td>
                            <a class="btn btn-primary" href="#"><span class="fa fa-edit"></span></a>
                            <a class="btn btn-danger" href="#"><span class="fa fa-trash"></span></a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        {{ $pelis->links() }}
    </main>
</body>
</html>