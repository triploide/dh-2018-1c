<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Movie;

class MovieController extends Controller
{
    public function index()
    {
        // $movie = Movie::find(7);
        // dd($movie->actors->pluck('first_name')->implode(', '));
        
        $movies = Movie::orderBy('title')->paginate(10);
        //dd($movies->pluck('title'));  //valor del array
        //dd($movies->pluck('title', 'id')); //valor + índice
        
        // $movies->each(function ($item) {
        //     echo $item->title . '<br>';
        // });
        
        // $nuevaCollection = $movies->map(function ($item) {
        //     return str_slug($item->title);
        // });
        
        // $nuevaCollection = $movies->filter(function ($item) {
        //     return $item->title != 'Test';
        // });

        //dd($movies->pluck('title', 'id'));
        //dd($movies->pluck('title', 'id'));
        
        return view('peliculas', ['peliculas' => $movies]);
    }
    
    public function mostrarForm()
    {
        $genres = \App\Genre::orderBy('name')->get();
        $actors = \App\Actor::orderBy('first_name')->limit(5)->get();
        return view('mostrar-form', ['genres' => $genres, 'actors' => $actors]);
    }
    
    public function guardarForm(Request $request)
    {
        //Store
        //$nombreImagen = $request->file('banner')->store('images');
        
        //-----------------
        //-----Request-----
        //-----------------
        //dd($request->all());
        //request()->input('release_date');
        //request()->except('_token', 'enviador');
        //request()->only('title', 'awards');
        //request()->all();
        
        
        //----------------------
        //-----Validaciones-----
        //----------------------
        //$this->validate(request, reglas);
        
        request()->validate([
            'title' => 'required|min:3|max:255|unique:movies',
            'awards' => 'integer',
            'release_date' => 'date',
            'banner' => 'image|max:2048|dimensions:ratio=16/9',
        ], [
            'title.required' => 'El título es obligatorio.'
        ]);
        
        //----------------------
        //-----Persistencia-----
        //----------------------
        /*
        $movie = new Movie;
        $movie->title = request()->input('title');
        $movie->rating = request()->input('rating');
        $movie->awards = request()->input('awards');
        $movie->release_date = request()->input('release_date');
        $movie->save();
        */
        
        if ($request->hasFile('banner')) {
            $nombreImagen = str_slug($request->input('title')) . '.' . $request->file('banner')->extension();
            $request->file('banner')->storeAs('images', $nombreImagen);
        }
        
        $movie = Movie::create(request()->all());
        $movie->actors()->sync(request()->input('actors'));
        
        return redirect('mostrar-imagen/' . $nombreImagen);
        
        //dd($movie->toArray());
        
        return 'bien';
    }
    
    public function mostrarImagen($src)
    {
        return view('mostrar-imagen', ['src' => $src]);
    }
    
}






