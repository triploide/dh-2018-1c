<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BuscarController extends Controller
{
    private $nombres = ['Barry', 'Barclay', 'Bert', 'Bort'];
    
    public function buscarNombre($busqueda)
    {
        $resultado = "El nombre no está en el array";
        
        foreach ($this->nombres as $key => $value) {
            if ($busqueda == $value) {
                $resultado = "$value está en la posición $key";
                break;
            }
        }
        
        return $resultado;
    }
}
