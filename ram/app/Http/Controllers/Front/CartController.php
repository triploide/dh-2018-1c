<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CartController extends Controller
{
    public function add($id)
    {
        if (
            !session()->has('productos')
            || !in_array($id, session()->get('productos'))
        ) {
            session()->push('productos', $id);
        }
        //return session()->get('productos');
        return redirect('carrito');
    }
    
    public function destroy()
    {
        session()->forget('productos');
        //return session()->get('productos');
        return redirect('carrito');
    }
    
    public function show()
    {
        $movies = [];
        
        if (session()->has('productos')) {
            $carrito = session()->get('productos');
            $movies = \App\Movie::whereIn('id', $carrito)->get();
        }
        
        return view('front.cart.show', ['movies' => $movies]);
    }
}
