<?php
function sumar($a, $b, $callback)
{
    sleep(3);
    $subtotal = $a + $b;
    $total = $callback($subtotal);
    return $total;
}
// 
// echo sumar(2, 3, function ($a, $b=2) {
//     return $a * $b;
// });

echo sumar(2, 3, function ($a) {
    return "El total es : $a";
});