<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class VistasController extends Controller
{
    public function contactenos()
    {
        return view('contactenos');
    }
    
    public function usuariosConectados()
    {
        $users = ['Daniel', 'Daniela', 'Flora', 'Bort'];
        return view('usuarios-conectados', ['usuarios' => $users]);
    }
}





