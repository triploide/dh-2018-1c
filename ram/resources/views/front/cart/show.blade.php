<!DOCTYPE html>
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if gt IE 9]> <html lang="en" class="ie"> <![endif]-->
<!--[if !IE]><!-->
<html lang="es">
    <!--<![endif]-->

    <head>
        <meta charset="utf-8">
        <title>Rent a Movie</title>
        <meta name="description" content="Mirá todas las películas y series desde la comodidad de tu casa. Rent a movie, compartiendo momentos.">
        @include('front.partials.head')
    </head>

    <body class="no-trans front-page">

        <!-- scrollToTop -->
        <!-- ================ -->
        <div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>
        
        <!-- page wrapper start -->
        <!-- ================ -->
        <div class="page-wrapper">
        
            @include('front.partials.header')
            
            <!-- breadcrumb start -->
            <!-- ================ -->
            <div class="breadcrumb-container">
                <div class="container">
                    <ol class="breadcrumb">
                        <li><i class="fa fa-home pr-10"></i><a href="">Home</a></li>
                        <li class="active">Series</li>
                    </ol>
                </div>
            </div>
            <!-- breadcrumb end -->
        
            <!-- main-container start -->
            <!-- ================ -->
            <section class="main-container">

                <div class="container">
                    <div class="row">

                        <!-- main start -->
                        <!-- ================ -->
                        <div class="main col-md-8">

                            <!-- page-title start -->
                            <!-- ================ -->
                            <h1 class="page-title">Carrito</h1>
                            <div class="separator-2"></div>
                            <!-- page-title end -->
                            
                            <p><a class="btn btn-danger" href="cart/destroy">Borrar Carrito</a></p>
                            
                            <table class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Título</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse ($movies as $movie)
                                        <tr>
                                            <td>{{ $movie->id }}</td>
                                            <td>{{ $movie->title }}</td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="2">No ha productos en el carrito</td>
                                        </tr>
                                    @endforelse
                                </tbody>
                            </table>

                        </div>
                        <!-- main end -->

                        <!-- sidebar start -->
                        <!-- ================ -->
                        <aside class="col-md-4 col-lg-3 col-lg-offset-1">
                            <div class="sidebar">
                                
                                @include("front.aside.buscar")
                                
                                @include("front.aside.categorias")

                                @include("front.aside.relacionados")
                                                              
                            </div>
                        </aside>
                        <!-- sidebar end -->

                    </div>
                </div>
            </section>
            <!-- main-container end -->


            <?php //include "tpl/components/calls-to-action/recomendaciones.php"; ?>
            
            @include('front.partials.footer')
            
        </div>
        <!-- page-wrapper end -->

        @include('front.partials.scripts')

    </body>
</html>
