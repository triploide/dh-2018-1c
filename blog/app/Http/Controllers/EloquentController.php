<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Movie;

class EloquentController extends Controller
{
    public function whereAnd()
    {
        $movies = Movie::where('genre_id', 5)->where('awards', '>', 3)->get();
        return $this->mostrarMovies($movies);
    }
    
    public function whereParentesis()
    {
        $movies = Movie::where(function ($query) {
            $query->where('release_date', '>=', "2008-01-01")
                ->where('release_date', '<=', '2008-12-31');
        })->orWhere(function ($query2) {
            $query2->where('awards', '>=', '3')
                ->where('awards', '<=', '5');
        })
        ->get()
        ;
        return $this->mostrarMovies($movies);
    }
    
    private function mostrarMovies($movies)
    {
        $resultado = '';
        foreach ($movies as $movie) {
            $resultado .= '<p>' . $movie->title . ' - premios:' . $movie->awards . '</p>';
        }
        return $resultado;
    }
    
    public function belongsTo()
    {
        $movie = Movie::find(68);
        dd($movie->genre->toArray());
        /*
        $genre = $movie->genre;
        dd($genre->movies->toArray());
        */
    }
    
    public function hasMany()
    {
        $genre = \App\Genre::find(7);
        dd($genre->movies->toArray());
    }
    
    public function belongsToMany()
    {
        $movie = Movie::find(3);
        echo $movie->title;
        dd($movie->actors->toArray());
    }
    
    public function relationQuery()
    {
        $genre = \App\Genre::find(7);
        $movies = $genre->movies()->where('rating', '>', 8)->get();
        dd($movies->toArray());
    }
}








