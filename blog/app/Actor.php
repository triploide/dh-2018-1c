<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Actor extends Model
{
    public function movies()
    {
        //return $this->belongsToMany(Actor::class, 'tabla_relacion');
        return $this->belongsToMany(Movie::class);
    }
}
